import { useEffect, useState } from "react";
import { DETAILS_LINKS_TO_RENDER } from "../constants";
import { DataInterface } from "@/pages/[page]/[id]";

const useFetchDetails = (data: DataInterface, error: Error | unknown) => {
  const [fieldNames, setFieldNames] = useState<DataInterface>({});

  const fetchDetails = async () => {
    const selectedDetails = { ...data };

    await Promise.all(
      DETAILS_LINKS_TO_RENDER.map(async (field) => {
        if (data[field]) {
          if (Array.isArray(data[field])) {
            const fieldData = await Promise.all(
              data[field].map(async (fieldValue: string) => {
                const response = await fetch(fieldValue);
                const responseData = await response.json();
                return responseData.name;
              })
            );
            selectedDetails[field] = fieldData.join(", ");
          } else {
            const response = await fetch(data[field]);
            const fieldData = await response.json();
            selectedDetails[field] = fieldData.name;
          }
        }
      })
    );

    setFieldNames(selectedDetails);
  };

  useEffect(() => {
    if (!data || error) {
      return;
    }

    fetchDetails();
  }, [data, error]);

  return fieldNames;
};

export default useFetchDetails;
