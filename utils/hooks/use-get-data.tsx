import { useState, useEffect } from "react";

interface FetchDataResult<T> {
  data: T;
  isLoading: boolean;
  error?: Error | unknown;
}

const useGetData = <T,>(url: string): FetchDataResult<T> => {
  const [data, setData] = useState<T>(null);
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [error, setError] = useState<Error | unknown>(null);

  const fetchData = async () => {
    setIsLoading(true);
    try {
      const response = await fetch(url);
      const result = await response.json();
      setData(result);
    } catch (err) {
      setError(err);
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    fetchData();
  }, [url]);

  return { data, isLoading, error };
};

export default useGetData;
