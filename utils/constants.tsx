export const PATHS = ["people", "vehicles", "planets"];

export const DETAILS_LINKS_TO_RENDER = ["homeworld", "vehicles", "residents", "pilots"];

export const IMAGE_PLACEHOLDER =
  "https://icon-library.com/images/no-image-icon/no-image-icon-0.jpg";
