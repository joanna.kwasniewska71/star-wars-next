export const getIdFromUrl = (url: string): number | null => {
  const idString = url
    .split("/")
    .filter((part) => part !== "")
    .pop();

  if (idString === undefined) {
    return null; 
  }

  const parsedId = parseInt(idString);

  if (isNaN(parsedId)) {
    return null;
  }

  return parsedId;
};
