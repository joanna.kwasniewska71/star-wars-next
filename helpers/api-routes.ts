// NOTE: This variables should be in .env file
const API_MAIN_ROUTE = "https://swapi.dev/api";
const API_IMAGES = "https://starwars-visualguide.com/assets/img";

const API_PEOPLE = `/people`;
const API_VEHICLES = `/vehicles`;
const API_PLANETS = `/planets`;

export { API_MAIN_ROUTE, API_VEHICLES, API_PEOPLE, API_PLANETS, API_IMAGES };
