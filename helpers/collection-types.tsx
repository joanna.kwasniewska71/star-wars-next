export const COLLECTION_TYPES_MAP: CollectionType = {
  people: "characters",
  vehicles: "vehicles",
  planets: "planets"
};

export interface CollectionType {
  [key: string]: string;
}
