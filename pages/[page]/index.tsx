import type { NextPage } from "next";
import { API_MAIN_ROUTE } from "@/helpers/api-routes";
import CustomError404 from "../error404";
import RecordsList from "@/components/RecordsList";
import useGetData from "@/utils/hooks/use-get-data";
import Spinner from "@/components/Spinner";

export interface DataCollectionProps {
  name: string;
  url: string;
}

const CollectionPage: NextPage = ({ type }) => {
  const url = `${API_MAIN_ROUTE}/${type}`;
  const { data, isLoading } = useGetData(url);

  if (!data && !isLoading) {
    return <CustomError404 />;
  }

  return (
    <>
      {isLoading ? (
        <Spinner />
      ) : (
        <section>
          <h1>{type.toUpperCase()} COLLECTION</h1>
          <RecordsList typeCollection={type} data={data} />
        </section>
      )}
    </>
  );
};

CollectionPage.getInitialProps = async ({ query }) => {
  const { page } = query;

  return { type: page };
};

export default CollectionPage;
