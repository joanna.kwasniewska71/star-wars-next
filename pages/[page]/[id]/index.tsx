import type { GetServerSideProps } from "next";
import { API_IMAGES, API_MAIN_ROUTE } from "@/helpers/api-routes";
import CustomError404 from "@/pages/error404";
import useGetData from "@/utils/hooks/use-get-data";
import Spinner from "@/components/Spinner";
import Link from "next/link";
import { useCallback } from "react";
import { DETAILS_LINKS_TO_RENDER } from "@/utils/constants";
import { COLLECTION_TYPES_MAP } from "@/helpers/collection-types";
import useFetchDetails from "@/utils/hooks/use-fetch-details";
import { REST_DETAILS } from "@/enums/rest-details";
import UiImage from "@/components/common/UIImage";

export interface DataInterface {
  [key: string]: string & string[] & URL & URL[];
}

type RecordCardProps = {
  page: string & keyof typeof COLLECTION_TYPES_MAP;
  id: number;
};

const RecordCard = ({ page, id }: RecordCardProps) => {
  const url = `${API_MAIN_ROUTE}/${page}/${id}`;
  const srcImage = `${API_IMAGES}/${COLLECTION_TYPES_MAP[page]}/${id}.jpg`;
  const { data, isLoading, error } = useGetData<DataInterface>(url);
  const fieldNames = useFetchDetails(data, error);
  const isError = (data?.detail || !data || error) && !isLoading;

  const getPathFromUrl = useCallback(
    (partRemove: string, fullUrl: string) => fullUrl?.replace(partRemove, ""),
    []
  );

  const renderFieldItems = (field: string) => {
    const fieldData: string | string[] = data[field];

    if (Array.isArray(fieldData)) {
      return fieldData?.map((url, index) => {
        const value = fieldNames[field]?.split(", ")[index];
        const lastItem = index === fieldData.length - 1;

        return (
          <Link key={index} href={getPathFromUrl(API_MAIN_ROUTE, url)}>
            {value}
            {!lastItem && ", "}
          </Link>
        );
      });
    } else if (fieldData) {
      return <Link href={getPathFromUrl(API_MAIN_ROUTE, fieldData)}>{fieldNames[field]}</Link>;
    }
  };

  const renderField = (field, index) => {
    const isEmptyField = fieldNames[field] === undefined;

    if (isEmptyField) {
      return null;
    }

    if (data[field]?.length === 0) {
      return (
        <span key={index}>
          <strong className="field-name">{field}:</strong>
          <span> no items</span>
        </span>
      );
    }

    return (
      <p key={index}>
        <strong className="field-name">{field}:</strong> {renderFieldItems(field)}
      </p>
    );
  };

  const renderRestDetails = (field: REST_DETAILS) => {
    if (data?.[field]) {
      const REGEX = /\B(?=(\d{3})+(?!\d))/g;
      const formatedNumber = data[field].toString().replace(REGEX, " ");

      return (
        <p>
          <strong className="field-name"> {field.replace("_", " ")}:</strong> {formatedNumber}
        </p>
      );
    }
  };

  if (isError) return <CustomError404 />;

  if (isLoading || !data) {
    return <Spinner />;
  }

  return (
    <>
      <section className="card">
        <UiImage src={srcImage} width={400} height={460} />
        <div className="card-info">
          <h1>{fieldNames?.name}</h1>
          <div>{DETAILS_LINKS_TO_RENDER.map((field, index) => renderField(field, index))}</div>
          {renderRestDetails(REST_DETAILS.POPULATION)}
          {renderRestDetails(REST_DETAILS.VEHICLE_CLASS)}
        </div>
      </section>
    </>
  );
};

export const getServerSideProps: GetServerSideProps = async ({ query }) => {
  const { id, page } = query;
  
  return { props: { page, id } };
};

export default RecordCard;
