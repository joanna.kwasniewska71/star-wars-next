import type { NextPage } from "next";
import RecordsList from "@/components/RecordsList";
import { API_MAIN_ROUTE, API_PEOPLE } from "@/helpers/api-routes";
import useGetData from "@/utils/hooks/use-get-data";
import CustomError404 from "./error404";

const HomePage: NextPage = () => {
  const url = `${API_MAIN_ROUTE}/${API_PEOPLE}`;
  const { data, isLoading } = useGetData(url);

  if (!data && !isLoading) {
    return <CustomError404 />;
  }
  
  return (
    <section>
      <h1>STARWARS COLLECTION</h1>
      <RecordsList data={data} />
    </section>
  );
};

export default HomePage;
