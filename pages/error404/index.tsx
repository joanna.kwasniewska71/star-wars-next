import React from "react";
import { NextPage } from "next";

const CustomError404: NextPage = () => {
  return (
    <section className="error-container">
      <h2>Oops! Page not found.</h2>
      <p>
        The requested page does not exist.
        <br /> Please check the URL and try again.
      </p>
    </section>
  );
};

export default CustomError404;
