import type { AppProps } from "next/app";
import Navigation from "@/components/common/Navigation";
import "@/styles/globals.css";

function App({ Component, pageProps }: AppProps) {
  return (
    <>
      <Navigation />
      <Component {...pageProps} />
    </>
  );
}

export default App;
