/** @type {import('next').NextConfig} */
const path = require("path");

module.exports = {
  images: {
    domains: ["starwars-visualguide.com", "icon-library.com"]
  },
  webpack: (config) => {
    config.resolve.alias["@"] = path.resolve(__dirname);
    return config;
  }
};
