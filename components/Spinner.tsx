import React, { useState, useEffect } from "react";

const Spinner = () => {
  const [dots, setDots] = useState("");
  const [addingDots, setAddingDots] = useState(true);

  useEffect(() => {
    const intervalId = setInterval(() => {
      if (addingDots) {
        setDots((prevDots) => (prevDots.length < 5 ? prevDots + "." : ""));
        if (dots.length === 5) {
          setAddingDots(false);
        }
      } else {
        setDots((prevDots) => prevDots.slice(0, -1));
        if (dots.length === 0) {
          setAddingDots(true);
        }
      }
    }, 100);

    return () => clearInterval(intervalId);
  }, [dots, addingDots]);

  return (
    <section>
      <div className="loader">Loading{dots}</div>
    </section>
  );
};

export default Spinner;
