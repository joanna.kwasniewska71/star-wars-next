import { API_IMAGES } from "@/helpers/api-routes";
import Link from "next/link";
import { COLLECTION_TYPES_MAP } from "@/helpers/collection-types";
import UiImage from "@/components/common/UIImage";

const Record = ({ id, type, name }) => {
  const src = `${API_IMAGES}/${COLLECTION_TYPES_MAP[type]}/${id}.jpg`;

  return (
    <Link href={`/${type}/${id}`}>
      <div className="record-list">
        <UiImage src={src} height={200} width={180} />
        <h3 className="record-name">{name}</h3>
      </div>
    </Link>
  );
};

export default Record;
