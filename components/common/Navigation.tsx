import { PATHS } from "@/utils/constants";
import Link from "next/link";
import { useRouter } from "next/router";

const Navigation = () => {
  const { query } = useRouter();

  return (
    <nav>
      {PATHS.map((path) => (
        <div key={path}>
          <Link href={`/${path}`}>
            <span
              style={{
                padding: "3px",
                color: query.page === `${path}` ? "yellow" : "wheat",
                borderBottom: query.page === `${path}` ? "3px solid yellow" : null,
                borderLeft: query.page === `${path}` ? "3px solid yellow" : null
              }}
            >
              {path.charAt(0).toUpperCase() + path.slice(1)}
            </span>
          </Link>
        </div>
      ))}
    </nav>
  );
};

export default Navigation;
