import { checkImageExists } from "@/utils/check-image-render";
import { IMAGE_PLACEHOLDER } from "@/utils/constants";
import Image from "next/image";
import { useEffect, useState } from "react";

const UiImage = ({ src, width = 100, height = 100 }) => {
  const [imageExists, setImageExists] = useState(true);

  useEffect(() => {
    checkImageExists(src).then((isExist) => {
      setImageExists(isExist);
    });
  }, [src]);

  if (imageExists) return <Image alt="photo" src={src} width={width} height={height} />;
  
  return <Image alt="photo" src={IMAGE_PLACEHOLDER} width={width} height={height} />;
};

export default UiImage;
