import { getIdFromUrl } from "@/utils/get-id-from-url";
import Record from "./Record";

const RecordsList = ({ typeCollection = "people", data }) => {
  const sortedResults = data?.results
    .slice()
    .sort((current, next) => current.name.localeCompare(next.name));

  return (
    <div className="records-wrapper">
      {sortedResults?.map(({ name, url }, key) => {
        const id = getIdFromUrl(url);

        return <Record key={key} id={id} type={typeCollection} name={name} />;
      })}
    </div>
  );
};

export default RecordsList;
