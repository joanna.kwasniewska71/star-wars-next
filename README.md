## My App - StarWars searcher 

This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).


## Requirements

    Node.js (recommended version: 20.x)
    npm (recommended version: 9.x)


## Installation

  1. Clone the repository: git clone `https://gitlab.com/joanna.kwasniewska71/star-wars-next`
  2. Navigate to the project folder: `cd star-wars-next`
  3. Install dependencies: npm install


## Getting Started

After installing the dependencies, you can run the application locally

```bash
npm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

Have fun :)


## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!


## Author

Joanna Kwaśniewska 


